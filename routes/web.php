<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\InicioController;
use App\Http\Controllers\RestController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [InicioController::class, 'welcome']);

Route::get('rest' , [RestController::class, 'inicio']);

Route::post ('rest/insertar',[RestController::class, 'store']);

Route::get('rest/cesta',[RestController::class, 'cesta']);

Route::post ('rest/add/{producto}',[RestController::class, 'add']);

Route::get('rest/{producto}', [RestController::class, 'show']);

Route::delete('rest/{producto}/borrar',[RestController::class, 'destroy']);







