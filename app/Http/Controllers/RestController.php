<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Producto;
use App\Models\Cesta;

class RestController extends Controller
{
    public function inicio(){

        $productos = Producto::all();

        //$productos = [['nombre'=>'producto1','precio' => 23],['nombre'=>'producto2','precio'=>44]];

        return response()->json($productos);

    }

    public function show($producto){

        $producto = Producto::find($producto);

        return response()->json($producto);
    }

    public function destroy($producto){

        $producto = Producto::find($producto);

        $producto->delete();

        return response()->json(['mensaje' => 'producto borrado']);
    }

    public function store(Request $request){

        $datos = $request->except('_token');
        $datos['nombre'] = $request->nombre;
        $datos['precio'] = $request->precio;

        try{
            $producto = Producto::create($datos);
            return response()->json(['mensaje' => 'producto subido correctamente']);
        }catch(Illuminate\Database\QueryException $ex){            
            return response()->json(['mensaje'=>'Fallo al subir el producto']);
        

        }   
    }

    public function add($producto_id){

        $data = [
            
            'producto_id' => $producto_id
        ];

        try{
            $producto = Cesta::create($data);
            return response()->json(['mensaje' => 'producto añadido a la cesta']);
        }catch(Illuminate\Database\QueryException $ex){
            return response()->json(['mensaje' => 'error al añadir a la cesta']);
        }
    }

    public function cesta(){

        $cesta = Cesta::all();

        return response()->json($cesta);
    }
}
